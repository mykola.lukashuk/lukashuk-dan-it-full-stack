//Опишите своими словами, как Вы понимаете, что такое обработчик событий.
// Штука що слухає що на сторінці і з тим щось може робити, при кліку, чи іншій події.

function createInput(){
    document.body.insertAdjacentHTML('afterbegin',`<div class="price-wrap" id="price"><div id="price-filter-parametrs" class="price-filter-parametrs"></div><div id="price-label-wrap"><label class="price-label">Текущая цена: $<input type="text" id="id-input" class="price-input" placeholder="Price"></label></div></div>`)
    let elem=document.getElementById('id-input');
}


document.addEventListener("DOMContentLoaded", () => {
    createInput();
    //  через те що все має через JS -- лишаю функцію в функціїї.
    document.getElementById("id-input").addEventListener("blur", myFunctionOut);

    function myFunctionOut() {
        let elem=document.getElementById('id-input');

        if (Number(elem.value)>0) { // читав, що якщо людина ввела менше 0, але тоді валідація на пусто, і так далі, тому спростив і 0 теж відсікаю.
            document.getElementById('price-filter-parametrs').insertAdjacentHTML("beforeend", `<div class="filter-element"><span>Текущая цена: ${Number(elem.value)}</span><button class="button-delete">x</button>  </div>`);
            document.getElementById('id-input').style.backgroundColor = 'green';
            if (document.getElementById('error-wrap')) {
                document.getElementById('error-wrap').remove();
                document.getElementById('id-input').classList.remove('bad-price');
            }
        } else{
            if (! document.getElementById('error-wrap')) {
                document.getElementById('price-label-wrap').insertAdjacentHTML("afterend", `<div id="error-wrap" class="error-wrap"><span>Please enter correct price</span></div>`);
                document.getElementById('id-input').classList.add('bad-price');
            }
        }


        document.getElementById('price-filter-parametrs').addEventListener("click", (event) => {
            if (event.target.tagName ==='BUTTON') {

                let deleteItem = event.target.closest('div');
                deleteItem.remove();
            }
        })



    }



});
