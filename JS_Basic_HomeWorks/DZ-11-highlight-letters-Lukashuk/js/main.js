// Почему для работы с input не рекомендуется использовать события клавиатуры?
// з мобільних різних клавіатур можна не спрацьовувати, чи з автозаповнення, чи з буферу обмігу вставлене


// читав, що Связка элементов с клавишами через дата-атрибуты.
// Зробив через Query All з Data-set  як в букварі
// а варіант витягнути    const array1 = document.querySelector(".btn-wrapper");
// потім let aaa= array1.children; -- і тоді порівнювати по .textContent -- це хардкор?

let prev = document.querySelector(".btn_wrapper").firstElementChild;

document.addEventListener('keyup', function(event) {
    let keysItems = document.querySelectorAll('[data-set]');

    for (const item of keysItems) {
        if (item.getAttribute('data-set').toLowerCase()===event.key.toLowerCase()){
            prev.classList.remove("blue");
            item.classList.add("blue");
            prev=item;
        }
    }
});

// з точки зору швидкості правильніше зчитати спочатку всі елементи в масив, потім при натисканні заходити в цикл лише якщо цей елемент є в масиві, але це другий if