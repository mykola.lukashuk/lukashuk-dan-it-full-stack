'use strict';
// Екранування потрібно для роботи з текстом, наприклад якщо в контенті є лапки і не заекранувати, то перемінна обріжеться раніше і не матиме весь контент.

let newUser;

function createNewUser(firstName="Mykola", lastName="Lukashuk"){
    newUser = {
        getlogin: function (){
            return (this.firstName[0].toLowerCase()+this.lastName.toLowerCase());
        },
        setNewFirstName: function (value){
            Object.defineProperty(newUser, 'firstName', {
                writable: true,
            });
            this.firstName=value;
        },
        setNewLastName: function (value){
            Object.defineProperty(newUser, 'lastName', {
                writable: true,
            });
            this.lastName=value;
        },
        getAge: function (){
            let dateNow = new Date();
            let age = dateNow.getFullYear()-this.birthday.getFullYear();

            if ( dateNow.getMonth() < this.birthday.getMonth() ) {
                age--;
            } else if ( dateNow.getMonth() == this.birthday.getMonth() )
                if ( dateNow.getDate() < this.birthday.getDate() ) {
                    age--;
                }
            return age;
        },
        getPassword: function (){
            return (this.firstName[0].toLowerCase()+this.lastName.toLowerCase()+this.birthday.getFullYear());
        },
    }

    //myUser.firstName = "sss";

    function setBirthday (){
        let birthday = prompt('Ваша дата рождения в формате dd.mm.yyyy');
        //let birthday='20.04.1992';
        let dateNow = new Date();

        // конкретно для цієї задачі ні toUTCString, ні toString, ні Date.parse(), ні formatDate, не зводило до вірного та питання валідації було б, тому вирішив так:
        let day=birthday.slice(0,2);
        let month=birthday.slice(3,5);
        let year=birthday.slice(6,10);

        // високосні, і ті де 30 чи 31 днів перевірку не додавав. Чи крапка чи дефіс не трекаю.
        while(Number.isNaN(Number.parseFloat(day))||day===null||Number.isNaN(Number.parseFloat(month))||month===null||Number.isNaN(Number.parseFloat(year))||year===null||Number(day)<1||Number(day)>31||Number(month)<1||Number(month)>12||Number(year)<1900||Number(year)>(dateNow.getFullYear()+1)){
            birthday = prompt('Ваша дата рождения в формате dd.mm.yyyy !!!');
            day=birthday.slice(0,2);
            month=birthday.slice(3,5);
            year=birthday.slice(6,10);
        }



        newUser.birthday = new Date(`${month}/${day}/${year}`);// console.log(`Введена дата в вірному форматі: ${birthdayObject.toString()}`);

    }

    setBirthday ();

    Object.defineProperty(newUser, 'firstName', {
        value: firstName,
        writable: false,
        configurable: true,
        // ...
    });
    Object.defineProperty(newUser, 'lastName', {
        value: lastName,
        writable: false,
        configurable: true,
        // ...
    });



    //myUser.firstName = "sss";
    return newUser;
}

let myUser = createNewUser(prompt("Ваше имя"), prompt("Ваша фамилия")); // Создали объект


console.log(myUser);
//myUser.firstName = "sss";
/console.log(`${newUser.getPassword()}`);
console.log(`Пользователю ${newUser.getAge()} полных лет`);
console.log(`${newUser.getPassword()}`);
