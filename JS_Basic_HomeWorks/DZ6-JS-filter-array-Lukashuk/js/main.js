// Опишите своими словами как работает цикл forEach.
// Перегрібає в масиві всі елементи, дозволяє простіше записувати пошук, не в вигляді for i =, і тоді звертатись до елемента, а більш зурчно де item сам елемент одразу




const arrDefault = ['string', 123, 'boolean', 233, 2111, true, false, 1, 0, 'true',[12,2], null, undefined];
const typeDefault = 'null';
const allTypes = ['string', 'number', 'boolean', 'bigint', 'null', 'undefined', 'symbol','object'];


// в першому передбачив object перевірку з null, без неї код був би return array1.filter(element => (typeof element) !== str);
// в другому оригінал
function filterBy(array1,str) {
    let newArray;
    if (str==='object') {newArray = array1.filter(element => (typeof element !== str) || (element === null));
    } else if (str==='null') {newArray = array1.filter(element => (typeof element !== str && element !== null ));
    } else{
     newArray = array1.filter(element => (typeof element) !== str);
    }
    return newArray;
}

function filterBy2(array2,str) {
    const newArray2 = [];
    array2.forEach(function(item, i, arr) {
        if ((typeof item) !== str) newArray2.push(item);
    });
    return newArray2;
}






// в такому випадку null якраз вибиває помилку і не йде до пункту 2 букваря по цій дз
if (!Array.isArray(arrDefault)) {
    throw new Error(`Це не масив`)
} else { if (!allTypes.includes(typeDefault)) {
    throw new Error(`Це не підтримуваний метод`)
} else {
    console.log(filterBy(arrDefault, typeDefault));
    console.log(filterBy2(arrDefault, typeDefault));
}
}
