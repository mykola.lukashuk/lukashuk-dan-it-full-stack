'use strict';
// Метод об'єкта - функція в об'єкті

function createNewUser0(firstName="Mykola", lastName="Lukashuk"){
    const newUser = {
        firstName: firstName,
        lastName: lastName,
        getlogin: function (){
            return (this.firstName[0].toLowerCase()+this.lastName.toLowerCase());
        }
    }
    return [newUser, newUser.getlogin()];
}
console.log(createNewUser0(prompt("Ваше имя"), prompt("Ваша фамилия")));
// це Базова версія, щоб запросити, запитати, і вивести: імя + прізвище + код функції через метод



// це розширена, щоб лише через методи змінювати і задавати:
let newUser;

// резервний метод черед функцію
/*function changeFirstName (firstName="Mykola") {
    Object.defineProperty(newUser, 'firstName', {
        writable: true,
        value: firstName,
        writable: false,
        // ...
    });
}

function changeLastName (lastName="Lukashuk") {
    Object.defineProperty(newUser, 'lastName', {
        writable: true,
        value: lastName,
        writable: false,
        // ...
    });
}*/

function createNewUser(firstName="Mykola", lastName="Lukashuk"){
    newUser = {
        getlogin: function (){
            return (this.firstName[0].toLowerCase()+this.lastName.toLowerCase());
        },

        set setNewFirstName (value){
            Object.defineProperty(newUser, 'firstName', {
                writable: true,
            });
            this.firstName=value;
        },

        set setNewLastName (value){
            Object.defineProperty(newUser, 'lastName', {
                writable: true,
            });
            this.lastName=value;
        }

    }

    Object.defineProperty(newUser, 'firstName', {
        value: firstName,
        writable: false,
        configurable: true,
        // ...
    });

    Object.defineProperty(newUser, 'lastName', {
        value: lastName,
        writable: false,
        configurable: true,
        // ...
    });

    return [newUser, newUser.getlogin()];
}

let myUser = createNewUser(prompt("Ваше имя"), prompt("Ваша фамилия")); // Создали объект
console.log ("Після першого присвоєння дані:");
console.log(newUser);


// Вибиває помилку, поідеї так і має, адже записувати не можна.
/*
newUser.firstName = "TEST1 перший що не записується";
newUser.lastName = "TEST2 перший що не записується";
console.log ("Після спроби криво переписати дані:");
console.log(myUser);
 */

// Не смотря на попытки перезаписать имя / фамилию -они остались старыми, так как writable: false

//
console.log(myUser);
console.log ("Після спроби вірно переписати дані:");

/*newUser.setNewFirstName = "TEST1 Name!";
newUser.setNewLastName = "TEST2 Surname!";*/

newUser.setNewFirstName = "First Name good change";
newUser.setNewLastName = "Last Name good change";
newUser.getlogin();
console.log(myUser);

// // окрім метода ще функція змінює:
// changeFirstName("Real Name Last Change");
// changeLastName("Real Last Name Last Change");

/*// Пытаемся перезаписать фамилию через метод
console.log(myUser);*/
