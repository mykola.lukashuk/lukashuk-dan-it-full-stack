// event.preventDefault щоб вимкнути звичайну дію, скажімо якби замість кнопки було посилання і хотіли задати логіку кнопки -- тоді б це прописали

function validPassword (){
    const pass1=document.querySelector(".password_input_1");
    const pass2=document.querySelector(".password_input_2");
    console.log(pass1.value);
    console.log(pass2.value);

    if (pass1.value!=="" && pass2.value!=="" && pass1.value===pass2.value) {
        {
            if(document.querySelector(".error_message")){
                document.querySelector(".error_message").remove();
            }

            alert("You are welcome;");
        }
    } else{
        if (!document.querySelector(".error_message")){
        pass2.insertAdjacentHTML('afterend',`<div style="color:red;" class="error_message"><span>Нужно ввести одинаковые непустые значения</span></div>`);
        }
    }
}

const showPassword = () =>{
    const formEl = document.querySelector('.password_form');
    formEl.addEventListener('click',function(event){
        if (event.target.tagName==="I"){
            if(event.target.classList.contains("fa-eye-slash")){
                event.target.parentElement.querySelector(".password_input").setAttribute("type", "password");
                event.target.classList.add("fa-eye");
                event.target.classList.remove("fa-eye-slash");
            } else {
                event.target.parentElement.querySelector(".password_input").removeAttribute("type");
                event.target.classList.remove("fa-eye");
                event.target.classList.add("fa-eye-slash");
            }
        }
        if (event.target.tagName==="BUTTON"){
            validPassword();

        }

    });
}
showPassword();

