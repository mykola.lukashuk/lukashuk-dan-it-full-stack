function tabs () {
    const tabMenu = document.querySelector('.tabs');
    tabMenu.addEventListener('click', event => {
        if (event.target.tagName ==='LI') {
            change(event);
        }
    });
    const change = (event) => {
        let menuItems = document.querySelectorAll('[data-tab]');


        for (const item of menuItems) {
            if (item.classList.contains('active')){
                item.classList.remove('active');


            }
        }


        if(!event.target.classList.contains('active')){
            event.target.classList.add('active');

        }
        let id = event.target.getAttribute('data-tab');
        let contentItems = document.querySelectorAll('.content-item');
        for (const item of contentItems) {
            if (item.classList.contains('active')){
                item.classList.remove('active');

            }
        }
        if(!document.getElementById(id).classList.contains('active')){
            document.getElementById(id).classList.add('active');

        }

    }



}

tabs();



