/**
 * Цикли
 * дозволяють
 * робити повторювальні дії з меншим куском коду та можливістю задавати логіку повторювання і виконання. **/
/** для оптимізації  ігноруємо пробіл, перевіряємо чи не пусте і все, купу пробілів ігнорую та вони тсануть нулем. **/
let a;

do{
    a = prompt("Введите число");
} while (Number.isNaN(Number.parseFloat(a))||a===null);

if (a<5) {
    console.log("Sorry, no numbers");
} else {
    for (let i=0; i<=a; i=i+5) {
       console.log(i);
    }
}

let m;
let n;

do {
    m = prompt("Введите меньшее число M");
    n = prompt("Введите большее число N");

    if (Number.isNaN(Number.parseFloat(m)) || m === null || Number.isNaN(Number.parseFloat(n)) || n === null || Number(m) >= Number(n)) {
        console.log("Ошибка, введите оба числа заново. Меньшее из введенных чисел должно быть m");
    }
} while (Number.isNaN(Number.parseFloat(m)) || m === null || Number.isNaN(Number.parseFloat(n)) || n === null || Number(m) >= Number(n));

console.log(`Прості числа від ${m} до ${n}:`);

for (let i = m; i<=n; i++){
    let j;
    for ( j = 2; j<i; j++){
        if (i % j === 0)  {
            break;
        }
    }
    /*console.log(` perep id i = ${i} j=${j} i%j- ${i%j}  j= ${j}  i= ${i} `);*/

    /* не стороге спеціально, бо тоді при меншому 2 не працює логіка. А так, дійшли до кінця - означає не було переривання, і яквно число попереднє ок */
    if (j == i)
        console.log(i);
}
