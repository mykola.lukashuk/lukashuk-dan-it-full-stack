const store = {
    apple: 8,
    beef: 162,
    banana: 14,
    chocolate: 0,
    milk: 2,
    water: 16,
    coffee: 0,
    tea: 13,
    cheese: 0,
};



function getItemBalance(item) {
    if (item in store) {
        console.log(` SSS ${item}: ${store[item]}`);
        return `${item}: ${store[item]}`;
    }

    return `${item}: not found`;
}

function getStoreBalance(list) {
    let result = '';

    if (!list.endsWith(',')) {
        list += ',';
    }

    while (list !== '') {
        const nextItem = list.slice(0, list.indexOf(','));

        result += getItemBalance(nextItem) + '\n';

        list = list.slice(list.indexOf(',') + 1);
    }

    return result;
}

console.log(getStoreBalance('apple,box,cheese,water,walter,milk'));

console.log( store.water);