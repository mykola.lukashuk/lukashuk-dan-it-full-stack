const gulp = require("gulp");

gulp.task('moveImg', () => {
     return gulp.src('src/img/*.jpg')
         .pipe(gulp.dest('dist/img'))
})

gulp.task('moveJs', () => {
     return gulp.src('src/js/*.js')
         .pipe(gulp.dest('dist/js'))
})

gulp.task('default', gulp.series ('moveImg','moveJs'));



