const gulp = require("gulp");

const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const cleancss = require('gulp-clean-css');
const del= require('del');



gulp.task('cleanDist', function () {
     return del([
          'dist/**/*',
          'dist'
     ]);
});

gulp.task('cleanCss', function () {
     return del([
          'dist/css/**/*',
          'dist/css/*',
          'dist/css'
     ]);
});

gulp.task('moveImg', () => {
     return gulp.src('src/img/*.jpg')
         .pipe(gulp.dest('dist/img'))
})

gulp.task('moveJs', () => {
     return gulp.src('src/js/*.js')
         .pipe(concat('scripts.min.js'))
         .pipe(uglify())
         .pipe(gulp.dest('dist/js'))
})


gulp.task ('moveCss', function(done) {
     return gulp.src('src/css/main.scss')
         .pipe(postcss([autoprefixer]))
         .pipe(cleancss())
         .pipe(concat('styles.min.css'))
         .pipe(gulp.dest('dist/css'))
     done();
}   );




gulp.task('watchCss', function () {
     gulp.watch(['./src/css/*.scss'], gulp.series ('cleanCss'));
});



gulp.task('build', gulp.series ('cleanDist','moveImg','moveJs','moveCss'));


gulp.task('default',  gulp.series ('build'));