# Gulp Starter
#### Версия NodeJS

`node -v`

Должна быть от 14 и выше версии

#### Версия Gulp

`gulp -v`

```
CLI version: 2.3.0
Local version: 4.0.2
```

CLI version это установка глобально пакета gulp-cli

`npm install --global gulp-cli`

### Ошибки которые могут возникнуть

1. Python

   ```
   npm ERR! command failed
   npm ERR! command C:\WINDOWS\system32\cmd.exe /d /s /c node build.js || nodejs build.js
   npm ERR! gyp info it worked if it ends with ok
   npm ERR! gyp info using node-gyp@8.4.1
   npm ERR! gyp info using node@16.13.1 | win32 | x64
   npm ERR! gyp ERR! find Python
   npm ERR! gyp ERR! find Python Python is not set from command line or npm configuration
   npm ERR! gyp ERR! find Python Python is not set from environment variable PYTHON
   ```

   Если вот такой кусок ошибки то у вас нету пакета Python его нужно скачать с оф. сайта и установить. (Ошибка была замечена на Windows OS)

   Download Python | Python.org  https://www.python.org/downloads/

2. Unsupported gulp version

   `gulp -v`

   ```
   CLI version: 2.3.0
   Local version: 1.0.0
   ```

   Нужно установить последнею версию gulp

   `npm i -D gulp`

   `gulp -v`

   Должен быть такой результат.

   ```
   CLI version: 2.3.0
   Local version: 4.0.2
   ```

### Рекомендации

Для изучение Gulp лучше сначала посмотреть уроки Ильи Кантора

https://learn.javascript.ru/screencast/gulp а уже потом смотреть остальные.
